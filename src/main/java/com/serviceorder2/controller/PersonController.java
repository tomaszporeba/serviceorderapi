package com.serviceorder2.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.serviceorder2.model.Person;
import com.serviceorder2.repository.PersonDao;

@Controller
@RequestMapping("/ServiceOrder")
@SessionAttributes("Person")
public class PersonController {
	
 
	@RequestMapping(value = "/addPerson", method = RequestMethod.GET)
	public String addPerson( Model model) {
		
		
		model.addAttribute("person", new Person());
		
			Person person = new Person();
			person.setName("Janusz");
			person.setSurname("teszjanusz");
			personDao.save(person);
		
		return "addPerson";

	}
	
	@RequestMapping(value = "/addPersonJson", method = RequestMethod.GET)
	public @ResponseBody Person addPersonJson(@ModelAttribute("person") Person person)

	{

		Person person1 = new Person();

		person1.setName("adam");
		person1.setSurname("dupka");

		return person1;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Model model){
		model.addAttribute("person", new Person());
		return "register";
	}
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addEntity(@Valid @ModelAttribute("person") Person person, BindingResult result)

	{
		System.out.println("bez errora");
		if(result.hasErrors())
		{
			System.out.println("error");
			return "addPerson";
		}
		else
		{
			personDao.save(person);
		}
		
		System.out.println("post : " + person.getName());
		return "redirect:index.html";
	}
	
	
	
	
	@Autowired
	private PersonDao personDao;
}
