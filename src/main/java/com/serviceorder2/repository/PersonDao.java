package com.serviceorder2.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.serviceorder2.model.Person;

@Transactional
public interface PersonDao extends CrudRepository<Person, Long>{

	public Person findByName(String name);
	
	
}
