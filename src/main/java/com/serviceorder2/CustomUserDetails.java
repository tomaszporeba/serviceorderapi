package com.serviceorder2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.serviceorder2.model.Person;
import com.serviceorder2.model.Role;

@SuppressWarnings("serial")
public class CustomUserDetails implements UserDetails {

	
	private String name;
	private String password;
	private String surname;
	Collection<? extends GrantedAuthority> authorities;
	
	
	public CustomUserDetails(Person byName){
		this.name = byName.getName();
		this.password = byName.getPassword();
		this.surname = byName.getSurname();
		
		
		List<GrantedAuthority> auths = new ArrayList<>();
		for (Role role : byName.getRoles())
			auths.add(new SimpleGrantedAuthority(role.getName().toUpperCase()));
		
		this.authorities = auths;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return name;
	}

	public String getSurname(){
		return surname;
	}
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
