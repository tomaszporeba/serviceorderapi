package com.serviceorder2;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.serviceorder2.model.Person;
import com.serviceorder2.model.Role;
import com.serviceorder2.repository.PersonDao;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
	
	
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }
	
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
    
    @Autowired
    public void authenticationManager(AuthenticationManagerBuilder builder, final PersonDao dao) throws Exception{
    	
    	Person person = new Person();
    	person.setName("mariusz");
    	person.setSurname("jakiś");
    	person.setPassword("password");
    	person.setRoles(Arrays.asList(new Role("USER"), new Role("ACTUATOR")));
    	dao.save(person);
    	builder.userDetailsService(new UserDetailsService() {
			
			@Override
			public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
			
				return new CustomUserDetails(dao.findByName(s));
			}
		});
    }
}
